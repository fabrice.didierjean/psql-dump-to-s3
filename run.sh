#!/bin/sh

for db in `psql --host $DB_HOST --port $DB_PORT --user $DB_USER -t -c "select datname from pg_database where not datistemplate" -d postgres | grep '\S' | awk '{$1=$1};1'`; do
   pg_dump -O -x --host $DB_HOST --port $DB_PORT --user $DB_USER $db | gzip > /data/$db.sql.gz
done

s3cmd --access_key=$SCW_ACCESS_ID --secret_key=$SCW_ACCESS_SECRET --host-bucket="%(bucket).s3.fr-par.scw.cloud" --host="s3.fr-par.scw.cloud" sync /data/ "s3://$SCW_BUCKET_NAME/`date '+%Y-%m-%d-%H-%M'`/"
rm -rf /data/*
