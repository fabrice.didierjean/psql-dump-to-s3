FROM alpine:3

RUN apk add s3cmd postgresql13-client

COPY run.sh /run.sh
RUN chmod +x /run.sh

RUN mkdir /data

CMD ["/run.sh"]
